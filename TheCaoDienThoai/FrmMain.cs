﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TheCaoDienThoai.Models;
using System.Globalization;
using DevExpress.XtraReports.UI;

namespace TheCaoDienThoai
{
    public partial class FrmMain : DevExpress.XtraEditors.XtraForm
    {
        TheCaoDbContext _context;
        public FrmMain()
        {
            InitializeComponent();
            _context = new TheCaoDbContext();
            NapDuLieu();
        }

        private void btnTaoHoaDon_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FrmTaoHoaDon frm = new FrmTaoHoaDon(this);
            frm.ShowDialog();
        }
        public void NapDuLieu()
        {
            try
            {
                _context = new TheCaoDbContext();
                var list = _context.HoaDons.ToList();
                gridControlThe.DataSource = list;
                gridViewThe.RefreshData();
                gridViewThe.BestFitColumns();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Đã xảy ra lỗi! " + Environment.NewLine + ex.ToString(), "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }

        }

        private void btnNapDuLieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NapDuLieu();
        }

        private void btnInHoaDon_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _context = new TheCaoDbContext();
            var vitri = (HoaDon)gridViewThe.GetFocusedRow();
            if (vitri == null) return;
            else
            {
                var hd = _context.HoaDons.Find(vitri.IDHoaDon);
                var report = new Reports.InHoaDonReport();
                report.NapDuLieu(hd);
                var printTool = new ReportPrintTool(report);
                printTool.Report.CreateDocument(true);
                printTool.ShowPreview();
            }

        }

        private void btnGioiThieu_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            XtraMessageBox.Show("Phần mềm hỗ trợ in hoá đơn thẻ cào - Phiên bản v1.0@" + DateTime.Now.Year + Environment.NewLine + "Phát triển bởi Virtus.Pro.KSi https://facebook.com/xuanh251", "Thông tin", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}