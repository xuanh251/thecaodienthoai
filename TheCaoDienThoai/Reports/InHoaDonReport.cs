﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using TheCaoDienThoai.Models;
using System.Globalization;

namespace TheCaoDienThoai.Reports
{
    public partial class InHoaDonReport : DevExpress.XtraReports.UI.XtraReport
    {
        public InHoaDonReport()
        {
            InitializeComponent();
        }
        public void NapDuLieu(HoaDon hd)
        {
            switch (hd.NhaMang)
            {
                case "Viettel":
                    xrPictureBoxlogo.Image = Properties.Resources.viettel;
                    break;
                case "Mobifone":
                    xrPictureBoxlogo.Image = Properties.Resources.mobi;
                    break;
                case "Vinaphone":
                    xrPictureBoxlogo.Image = Properties.Resources.vinaphone;
                    break;
                case "Gate":
                    xrPictureBoxlogo.Image = Properties.Resources.Gate;
                    break;
                case "Garena":
                    xrPictureBoxlogo.Image = Properties.Resources.garena;
                    break;
                case "Zing":
                    xrPictureBoxlogo.Image = Properties.Resources.zingn;
                    break;
                default:
                    xrPictureBoxlogo.Image = Properties.Resources.VTC;
                    break;
            }
            xrLabelMenhGia.Text = "Thẻ cào " + hd.NhaMang + " " + double.Parse(hd.GiaTien.ToString()).ToString("#,###", CultureInfo.GetCultureInfo("vi-VN").NumberFormat) + " đồng";
            xrTableCellMaThe.Text = hd.MaNapTien;
            xrLabelSeri.Text = hd.SoSeri;
            xrLabelNgayHetHan.Text = hd.NgayHetHan.ToString("dd/MM/yyyy");
            xrLabelThoiGianXuat.Text = hd.NgayTao.ToString("dd/MM/yyyy HH:mm:ss");
            xrLabelMaPhieu.Text = hd.IDHoaDon.ToString();
        }
    }
}
