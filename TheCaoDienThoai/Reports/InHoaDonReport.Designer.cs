﻿namespace TheCaoDienThoai.Reports
{
    partial class InHoaDonReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InHoaDonReport));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLabelSeri = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelMaPhieu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelNgayHetHan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelSerial = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCellMaThe = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelThoiGianXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBoxlogo = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabelMenhGia = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.filteringUIContext1 = new DevExpress.Utils.Filtering.FilteringUIContext(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filteringUIContext1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelSeri,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel11,
            this.xrLabelMaPhieu,
            this.xrLabel6,
            this.xrLabelNgayHetHan,
            this.xrLabel2,
            this.xrLabelSerial,
            this.xrTable1,
            this.xrLabel4,
            this.xrLabelThoiGianXuat,
            this.xrPictureBoxlogo,
            this.xrLabel1,
            this.xrLabelMenhGia});
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 656.5857F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.StylePriority.UsePadding = false;
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabelSeri
            // 
            this.xrLabelSeri.Dpi = 254F;
            this.xrLabelSeri.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelSeri.LocationFloat = new DevExpress.Utils.PointFloat(138.9882F, 241.6396F);
            this.xrLabelSeri.Name = "xrLabelSeri";
            this.xrLabelSeri.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelSeri.SizeF = new System.Drawing.SizeF(385.9561F, 44.3089F);
            this.xrLabelSeri.StylePriority.UseFont = false;
            this.xrLabelSeri.Text = "xrLabel3";
            // 
            // xrLabel10
            // 
            this.xrLabel10.Dpi = 254F;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(92.56246F, 429.5516F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(432.3818F, 42.85635F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.Text = "- Thẻ đã mua sẽ không được hoàn trả";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 7F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(24.47647F, 391.3645F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(130.5278F, 38.18713F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Lưu ý:";
            // 
            // xrLabel8
            // 
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 7F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(24.47647F, 532.3841F);
            this.xrLabel8.Multiline = true;
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(500.468F, 58.42004F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Thẻ được xuất từ: https://www.facebook.com/thieuwalker1\r\n";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Dpi = 254F;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(92.56246F, 472.4079F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(432.3819F, 42.85614F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.Text = "- Chỉ hỗ trợ xuất hoá đơn trong ngày";
            // 
            // xrLabelMaPhieu
            // 
            this.xrLabelMaPhieu.Dpi = 254F;
            this.xrLabelMaPhieu.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabelMaPhieu.LocationFloat = new DevExpress.Utils.PointFloat(61.9539F, 602.087F);
            this.xrLabelMaPhieu.Name = "xrLabelMaPhieu";
            this.xrLabelMaPhieu.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelMaPhieu.SizeF = new System.Drawing.SizeF(462.9904F, 33.12903F);
            this.xrLabelMaPhieu.StylePriority.UseFont = false;
            this.xrLabelMaPhieu.StylePriority.UseTextAlignment = false;
            this.xrLabelMaPhieu.Text = "xrLabelMaPhieu";
            this.xrLabelMaPhieu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(13.22917F, 602.087F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(48.72473F, 33.12897F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "ID:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelNgayHetHan
            // 
            this.xrLabelNgayHetHan.Dpi = 254F;
            this.xrLabelNgayHetHan.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabelNgayHetHan.LocationFloat = new DevExpress.Utils.PointFloat(191.7519F, 339.8032F);
            this.xrLabelNgayHetHan.Name = "xrLabelNgayHetHan";
            this.xrLabelNgayHetHan.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelNgayHetHan.SizeF = new System.Drawing.SizeF(333.1928F, 36.63074F);
            this.xrLabelNgayHetHan.StylePriority.UseFont = false;
            this.xrLabelNgayHetHan.Text = "xrLabelNgayHetHan";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(24.47647F, 300.0596F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(167.2755F, 39.74356F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Thời gian xuất:";
            // 
            // xrLabelSerial
            // 
            this.xrLabelSerial.Dpi = 254F;
            this.xrLabelSerial.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabelSerial.LocationFloat = new DevExpress.Utils.PointFloat(24.07687F, 241.6396F);
            this.xrLabelSerial.Name = "xrLabelSerial";
            this.xrLabelSerial.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelSerial.SizeF = new System.Drawing.SizeF(114.9113F, 44.30893F);
            this.xrLabelSerial.StylePriority.UseFont = false;
            this.xrLabelSerial.Text = "Serial:";
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(24.07687F, 167.6358F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(501.7911F, 49.49268F);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCellMaThe});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCellMaThe
            // 
            this.xrTableCellMaThe.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCellMaThe.Dpi = 254F;
            this.xrTableCellMaThe.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCellMaThe.Name = "xrTableCellMaThe";
            this.xrTableCellMaThe.StylePriority.UseBorders = false;
            this.xrTableCellMaThe.StylePriority.UseFont = false;
            this.xrTableCellMaThe.StylePriority.UseTextAlignment = false;
            this.xrTableCellMaThe.Text = "xrTableCellMaThe";
            this.xrTableCellMaThe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCellMaThe.Weight = 1D;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(24.07688F, 339.8031F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(167.6751F, 36.6308F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "Ngày hết hạn:";
            // 
            // xrLabelThoiGianXuat
            // 
            this.xrLabelThoiGianXuat.Dpi = 254F;
            this.xrLabelThoiGianXuat.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabelThoiGianXuat.LocationFloat = new DevExpress.Utils.PointFloat(191.752F, 300.0595F);
            this.xrLabelThoiGianXuat.Name = "xrLabelThoiGianXuat";
            this.xrLabelThoiGianXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelThoiGianXuat.SizeF = new System.Drawing.SizeF(333.1928F, 39.74362F);
            this.xrLabelThoiGianXuat.StylePriority.UseFont = false;
            this.xrLabelThoiGianXuat.Text = "xrLabel3";
            // 
            // xrPictureBoxlogo
            // 
            this.xrPictureBoxlogo.Dpi = 254F;
            this.xrPictureBoxlogo.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBoxlogo.Image")));
            this.xrPictureBoxlogo.LocationFloat = new DevExpress.Utils.PointFloat(25.00003F, 11.8492F);
            this.xrPictureBoxlogo.Name = "xrPictureBoxlogo";
            this.xrPictureBoxlogo.SizeF = new System.Drawing.SizeF(173.1196F, 110.2783F);
            this.xrPictureBoxlogo.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(203.8316F, 11.8492F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(329.1684F, 63.71165F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Thẻ cào Thiệu Lê";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabelMenhGia
            // 
            this.xrLabelMenhGia.Dpi = 254F;
            this.xrLabelMenhGia.Font = new System.Drawing.Font("Times New Roman", 7F);
            this.xrLabelMenhGia.LocationFloat = new DevExpress.Utils.PointFloat(203.8316F, 87.05312F);
            this.xrLabelMenhGia.Name = "xrLabelMenhGia";
            this.xrLabelMenhGia.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabelMenhGia.SizeF = new System.Drawing.SizeF(329.1684F, 35.07438F);
            this.xrLabelMenhGia.StylePriority.UseFont = false;
            this.xrLabelMenhGia.StylePriority.UseTextAlignment = false;
            this.xrLabelMenhGia.Text = "Mã nạp tiền Viettel 200.000đ";
            this.xrLabelMenhGia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // InHoaDonReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Dpi = 254F;
            this.Margins = new System.Drawing.Printing.Margins(0, 4, 657, 0);
            this.PageHeight = 2000;
            this.PageWidth = 537;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.SnapGridSize = 25F;
            this.Version = "17.1";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filteringUIContext1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBoxlogo;
        private DevExpress.Utils.Filtering.FilteringUIContext filteringUIContext1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabelMenhGia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabelMaPhieu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabelNgayHetHan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabelSerial;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCellMaThe;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabelThoiGianXuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabelSeri;
    }
}
