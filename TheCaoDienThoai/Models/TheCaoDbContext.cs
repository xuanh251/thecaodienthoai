namespace TheCaoDienThoai.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data;

    public partial class TheCaoDbContext : DbContext
    {
        public TheCaoDbContext()
            : base("name=TheCaoDbContext")
        {
        }
        public DbSet<HoaDon> HoaDons { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
        }
    }
}
