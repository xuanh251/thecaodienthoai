﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheCaoDienThoai.Models
{
    public class HoaDon
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid IDHoaDon { get; set; }
        public string MaNapTien { get; set; }
        public float GiaTien { get; set; }
        public string NhaMang { get; set; }
        public string SoSeri { get; set; }
        public DateTime NgayTao { get; set; }
        public DateTime NgayHetHan { get; set; }
    }
}
