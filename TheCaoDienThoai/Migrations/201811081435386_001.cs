namespace TheCaoDienThoai.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _001 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.HoaDons");
            AlterColumn("dbo.HoaDons", "IDHoaDon", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.HoaDons", "IDHoaDon");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.HoaDons");
            AlterColumn("dbo.HoaDons", "IDHoaDon", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.HoaDons", "IDHoaDon");
        }
    }
}
