namespace TheCaoDienThoai.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HoaDons",
                c => new
                    {
                        IDHoaDon = c.Guid(nullable: false),
                        MaNapTien = c.String(),
                        GiaTien = c.String(),
                        NhaMang = c.String(),
                        SoSeri = c.String(),
                        NgayTao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.IDHoaDon);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.HoaDons");
        }
    }
}
