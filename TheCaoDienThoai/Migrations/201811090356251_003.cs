namespace TheCaoDienThoai.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _003 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HoaDons", "NgayHetHan", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HoaDons", "NgayHetHan");
        }
    }
}
