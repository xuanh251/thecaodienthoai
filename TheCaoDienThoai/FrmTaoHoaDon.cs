﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TheCaoDienThoai.Models;
using System.Globalization;
using DevExpress.XtraReports.UI;

namespace TheCaoDienThoai
{
    public partial class FrmTaoHoaDon : DevExpress.XtraEditors.XtraForm
    {
        private FrmMain _frmMain;
        TheCaoDbContext _context;
        Guid idphieu = Guid.Empty;
        public FrmTaoHoaDon(FrmMain frmMain)
        {
            _frmMain = frmMain;
            InitializeComponent();
            comboBoxEditNhaMang.EditValue = "Viettel";
            _context = new TheCaoDbContext();
        }

        private void FrmTaoHoaDon_Load(object sender, EventArgs e)
        {

        }

        private void comboBoxEditNhaMang_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoxEditNhaMang.EditValue.ToString())
            {
                case "Viettel":
                    pictureBoxNhaMang.Image = Properties.Resources.viettel;
                    break;
                case "Mobifone":
                    pictureBoxNhaMang.Image = Properties.Resources.mobi;
                    break;
                case "Vinaphone":
                    pictureBoxNhaMang.Image = Properties.Resources.vinaphone;
                    break;
                case "Gate":
                    pictureBoxNhaMang.Image = Properties.Resources.Gate;
                    break;
                case "Garena":
                    pictureBoxNhaMang.Image = Properties.Resources.garena;
                    break;
                case "Zing":
                    pictureBoxNhaMang.Image = Properties.Resources.zingn;
                    break;
                default:
                    pictureBoxNhaMang.Image = Properties.Resources.VTC;
                    break;
            }
        }

        private void btnHoanThanh_Click(object sender, EventArgs e)
        {
            try
            {
                float menhgia = float.Parse(comboBoxEditMenhGia.EditValue.ToString().Split('k')[0]) * 1000;
                var hoadon = new HoaDon()
                {
                    NhaMang = comboBoxEditNhaMang.EditValue.ToString(),
                    GiaTien = menhgia,
                    MaNapTien = textEditMaThe.Text,
                    SoSeri = textEditSeri.Text,
                    NgayTao = DateTime.Now,
                    NgayHetHan = DateTime.Now.AddYears(2)
                };
                _context.HoaDons.Add(hoadon);
                _context.SaveChanges();
                string GiaTienVND = double.Parse(hoadon.GiaTien.ToString()).ToString("#,###", CultureInfo.GetCultureInfo("vi-VN").NumberFormat) + " đồng";
                string NgayTao = hoadon.NgayTao.ToString("dd/MM/yyyy HH:mm:ss");
                memoEditThongTin.Text = "Hoá đơn: " + hoadon.IDHoaDon + " - Ngày tạo: " + NgayTao + Environment.NewLine + "Nhà mạng: " + hoadon.NhaMang + " - Mệnh giá: " + GiaTienVND + Environment.NewLine + "Số seri: " + hoadon.SoSeri + Environment.NewLine + "Mã thẻ: " + hoadon.MaNapTien + Environment.NewLine + "Hạn sử dụng: " + hoadon.NgayTao.AddYears(2).ToString("dd/MM/yyyy");
                XtraMessageBox.Show("Đã tạo hoá đơn thành công. Kiểm tra lại thông tin hoá đơn rồi tiến hành in hoá đơn.");
                idphieu = hoadon.IDHoaDon;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Đã xảy ra lỗi! " + Environment.NewLine + ex.ToString(), "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FrmTaoHoaDon_FormClosed(object sender, FormClosedEventArgs e)
        {
            _frmMain.NapDuLieu();
        }

        private void FrmTaoHoaDon_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            _context = new TheCaoDbContext();
            var hd = _context.HoaDons.Find(idphieu);
            if (hd != null)
            {
                var report = new Reports.InHoaDonReport();
                report.NapDuLieu(hd);
                var printTool = new ReportPrintTool(report);
                printTool.Report.CreateDocument(true);
                printTool.ShowPreview();
            }
            else
            {
                XtraMessageBox.Show("Không tìm thấy hoá đơn!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}